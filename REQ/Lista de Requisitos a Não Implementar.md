**Lista Completa de Requisitos a Não Implementar**

2. 4.1	Deverá ser possível escolher o branch que está ser visualizado. Por defeito será o main branch. (a visualização por branch será um requisito avançado).

6. 4.1	Cada novo acesso à dashboard deve envolver a recolha dos dados de actividade mais recente no repositório.

7. 4.1	O momento da última actualização dos dados deverá estar sempre presente no écran (e.g.“Última actualização há 3 minutos”).

9. 4.2	Os ficheiros presentes em cada directoria/pasta deverão poder ser visíveis (nome e extensão dos ficheiros)

10. 4.2	Cada categoria de ficheiro (REQ, DESIGN, CODE, TEST, CONFIG,...) deve ser distinguível dos restantes (vista por categoria)

11. 4.2	Outra vista dos ficheiros no repositório deverá ser por ‘churn’ i.e. intensidade das alterações, desde red hot (muito alterado) a light grey (muito pouco alterados). A grading scale deve ser relativa e não absoluta.

12. 4.2	Clicando num ficheiro deverá ser possível visualizar o seu conteúdo naquele commit.

13. 4.2	Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por categoria (REQ, DEV, TST,…)

14. 4.2	Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por autor. Clicando no ficheiro navegamos para a vista da história desse ficheiro, clicando no autor navegamos para a vista de ficheiros alterados por esse autor.

15. 4.2	Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por intensidade de alterações. Clicando no ficheiro navegamos para a vista do histórico desse ficheiro, clicando num autor (de uma alteração) navegamos para a vista de ficheiros modificados/editados por esse autor.

16. 4.3	Deverá ser possível conhecer a história de cada ficheiro, isto é: A lista de commits que o alterou (e quem) em cada commit; A dimensão das alterações (nr de linhas alteradas/acrescentadas/removidas) em cada commit; A existência de defeitos pendentes.

17. 4.3	Deverá ser possível saber, por ficheiro, quais os issues que lhe estão associados. Deverá ser possível visualizar a timeline de issues associados ao ficheiro e consultar cada issue.

18. 4.3	A informação estática sobre o ficheiro: De que tipo (REQ, TST, …), dimensão (naquele momento), métricas de estabilidade e, se for código, a sua complexidade.

22. 4.4.1 A evolução de esforço aplicado por categoria (REQ, PM, DEV, TST…) deve ser visualizável.

24. 4.4.2 Deve ser possível ver a lista de contribuições de cada membro por categoria (REQ, PM, DEV, TST...).

25. 4.4.2 Deve ser possível ver o papel de cada membro a partir da intensidade das suas contribuições por categoria.

27. 4.4.2 Deve ser possível ver a timeline de commits de cada membro, e também que ficheiros foram modificados em cada commit.

29. 4.6 Cada página deve ter uma timeline que permita ver o estado do repositório em cada instante.

30. 4.6 Deve ser possível ver a evolução da árvore de ficheiros ao longo do tempo.

31. 4.6 Deve ser possível ver quais os ficheiros foram modificados em cada commit e o seu autor.

35. 5 A dashboard deve suportar multi-idioma.
