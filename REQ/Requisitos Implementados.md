**REQUISITOS A IMPLEMENTAR**

- **NºREQ  Subtema          REQ**

**1**	 4.1	Deverá ser possível escolher o repositório GitLab a analisar.

**3**	 4.1	A visualização temporal (linha de tempo) deverá ser uniforme, i.e. as unidades de tempo devem corresponder ao mesmo comprimento na sua visualização. 

**3a**: A visualização da timeline deve ser gradual, o espaçamento entre commits deve ser possível de ver em unidades de tempo, nomeadamente em dias.

**5**	 4.1	Deverá existir funcionalidade ‘back’, i.e. regressar à vista anteriormente apresentada sem ter de reinserir as opções que levaram à sua visualização.

**19**	 4.4.1	Deve existir uma página com a lista de membros activos do repositório (membros que fizeram pelo menos um commit).

**20** 	 4.4.1	As contribuições de cada membro por categoria (REQ, PM, DEV, TST…) devem ser visualizáveis.

**32**	 5	Apenas utilizadores autenticados podem aceder a informação do projecto.

**34**   5	Deve ser possível navegar entre as diversas vistas e items de cada vista.

**36**	 5	A solução deve ser web-based.

**37**	 5	A tecnologia deve ser baseada em Python.

**38**		O utilizador deverá ser capaz de fazer login no website.

**39**		Padronização dos nomes e estrutura dos issues no repo.



**RESPETIVOS USER STORIES**

**1**  Como **membro ativo do projeto**, devo poder **escolher o repositório Gitlab** a analisar.	 

**3**  Como **membro ativo da equipa**, deverei ter **acesso a visualização temporal** (linha de tempo), a qual **deverá ser uniforme**, por exemplo as **unidades de tempo devem corresponder ao mesmo comprimento** na sua visualização, para questões de organização do projeto.

**3a**  A **visualização da timeline deve ser gradual**, o espaçamento entre commits deve ser possível de ver em **unidades de tempo**, nomeadamente em **dias**.

**5**  Como **membro ativo da equipa** quero que **exista a funcionalidade ‘back’** para que seja possível** regressar à vista anteriormente apresentada** sem ter de **reinserir as opções que levaram à sua visualização**.

**19**  Como** membro da equipa** quero uma **página com a lista de membros activos do repositório** de forma a que sejam facilmente **identificáveis os autores** do projeto.

**20**  Como **membro da equipa backend** quero que as **contribuições de cada membro por categoria sejam visualizáveis**.

**32**  Como** membro da equipa de backend** deve ser **permitido que apenas utilizadores autenticados** podem **aceder a informação do projecto**.

**34**  Como **membro ativo do projeto**, devo **poder navegar livremente entre as diversas vistas do projeto**, e **itens de cada vista**.

**36**  Como **membro ativo do projeto**, pretendo que a **solução seja web-based.**

**37**  Como **membro da equipa** quero a **tecnologia com base em Python** para uma **maior facilidade na construção de um site** onde o seu **principal objetivo será analisar data**.

**38**  Como **utilizador** deverei ser **capaz de fazer login na página do website**, sendo **possível permitir a minha autenticação**.

**39**  Como **membro da equipa de Qualidade**, quero ver os **issues** do projeto a **respeitar as normas** definidas no** processo "Template Issues e Commits"**, para facilitar a rastreabilidade dos mesmos. 



