from django import forms
from django.forms import fields, widgets
from django.contrib.auth.models import User

import datetime

class LoginForm(forms.Form):

    username = forms.CharField()

    password = forms.CharField(widget=forms.PasswordInput())

class SignUpForm(forms.ModelForm):
    class Meta:
        model= User
        fields=('username','password')
        widgets={'password':forms.PasswordInput()}

# date form to get data interval
class DateForm(forms.Form):
    date1 = forms.DateField(initial = datetime.date.today)
    date2 = forms.DateField(initial = datetime.date.today)    
