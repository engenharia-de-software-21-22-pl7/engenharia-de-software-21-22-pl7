#### Onde encontrar ficheiros no repo do projeto «DEI DREAM»
|Ficheiro| Local onde encontrar | Link
|--|--|--|
|**Atas**  |  Encontradas a partir do **_board_ «Atas»** acedido a partir dos **issues**  | [**Atas**](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/boards/3409039) 
| | **Ou**, dá para aceder a partir da **_label_ «ata»** ||
|**Perfis** | **Repositório** -> **Ficheiros** (do _main branch_) -> **Pasta «PM»** -> **Pasta «Perfis»** | [Perfis](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/tree/main/PM/Perfis)
| **Design Visual** | **Repositório** -> **Ficheiros** -> **Pasta «REQ»** | [Design Visual](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/tree/main/REQ)
| **Arquitetura** | **Repositório** -> **Ficheiros** -> **Pasta «ARCH»** | [Arquitetura](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/tree/main/ARCH)
|**Implementação**| **Repositório** -> **Ficheiros** -> **Pasta «DEV»** | [Implementação](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/tree/main/DEV)
|**Testes**|**Repositório** -> **Ficheiros** -> **Pasta «REQ»** |[Testes](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/tree/main/REQ)
|**Configurações CI CD Pipeline e IDE** | **Repositório** -> **Ficheiros** -> **Pasta «ENV»** | [Configurações CI CD Pipeline e IDE](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/tree/main/ENV)
|**Dashboards**|**Repositório** -> **Ficheiros** -> **Pasta «PM»** | [Dashboards](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/tree/main/PM)
|**Riscos**|**Repositório** -> **Ficheiros** -> **Pasta «PM»** | [Dashboards](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/tree/main/PM)
|**Produto Final**| **Repositório** -> **Ficheiros** -> **Pasta «PROD»** | [Produto Final](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/tree/main/PROD)
