Luís André Vasconcelos Franco Esteves Costa

2019210154

Equipa de Qualidade

uc2019210154@student.uc.pt

Licenciatura em Design e Multimédia

**Contribuições:**

- [Link para todas as tarefas realizadas por mim](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues?scope=all&state=all&author_username=liebsturl)

~PROC
- [Processo Issues e Commits](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/50)
- [Processo Labels](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/130)

~QA
- [Testagem e Criacao de Novos Testes Parte 2](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/331) 
- [PDF com os testes criados + testes já feitos pela PL5 e o resultado da sua revisão](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/merge_requests/103/diffs)
- [Revisao Issues](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/293)
- [Revisão Perfis Pessoais](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/125)

~REQ
- [User Story 37](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/128)
- [User Story 35](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/91)

~ENV
- [Estudo GitLab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/14)
 - [Estudo Python](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/144)

~PM
- [Atualizacao Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/339)
- [Presenca Reunioes](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/220)
- [Ata reunião 16-10-2021](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/78)
- [Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/38)
- [Atividade Geral](https://gitlab.com/users/liebsturl/activity)


Fui membro da equipa de qualidade, tive o papel de criar processos para os quais o resto das equipas deveria seguir, fui encarregue de rever uma das tabelas de validações de requesitos da PL5 e incluir novos testes se assim concluísse como necesário, entre outras tarefas de outras categorias.
