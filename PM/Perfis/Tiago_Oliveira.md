# **Perfil de Tiago Oliveira**

**Nome:** Tiago Miguel Gomes Marques Oliveira

**Número de estudante:** 2019219068

**Funções:**

    - Teachnical Leader da Equipa de Backend
    - Backend Developer
    - Arquiteto

**Email:** uc2019219068@student.uc.pt

**Curso:** Licenciatura em Engenharia Informática

<br>

## **Lista de Issues**

~ARCH

- [Arquitetura C4](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/116)
- [Primeiro Nivel Arquitetura C4](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/154)
- [Segundo Nivel Arquitetura C4](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/174)
- [Terceiro Nivel Arquitetura C4](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/210)

~DEV

- [Processo de Execucao do Programa Sem BD](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/251)
- [Implementacao: Contribuicoes de Cada Membro UST20](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/298)

~ENV

- [Estudo GitLab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/13)
- [Estudo Django](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/64)
- [Estudo Pytest](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/205)

~PM

- [Perfis Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/44)
- [Presenca Reuniões](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/215)
- [Ata Sessao de Trabalho 01/12/2021](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/278)
- [Atualizacao Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/306)

~REQ

- [User Story 26](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/105)
- [User Story 27](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/123)

<br>

## **Papel no Projeto**

Durante a realização do projeto fiquei responsável por ser Teachnical Leader da Equipa de Backend e pelo desenvolvimento da Arquitetura.

Nas primeiras semanas estudei as ferramentas, tecnologias necessárias para a implementação do projeto e por escolher de entre os modelos existentes, tendo escolhido desenvolver uma Arquitetura com o Modelo C4.

Na parte final do semestre fiz a comunicação da equipa de backend e frontend, resolvendo vários problemas relativos à ativação do Virtual Environment (venv) para as implementações necessárias, fiz um tutorial com todos os passos necessários para correr o programa e um tutorial de como configurar a base de dados.

Fiquei ainda responsável pela implementação do requisito correspondente ao **UST20**.
