**Maria Antónia Resende Torres** 2019211685

Equipa de **Requisitos**

email: uc2019211685@student.uc.pt

Lincenciatura em Design e Multimédia

**Contribuições**:

~QA

- [Revisão de Issues;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/338)

~PM

- [Perfil Pessoal;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/34)
- [Presenca Reunioes;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/234)
- [Ata Reunião 23/11/2021;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/249)
- [Atualizaçao Perfil Pessoal;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/337)

~REQ

- [Revisão Issues REQ;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/338)
- [Verificação dos Requisitos PL5;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/335)
- [User Story 36;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/289)
- [User Story 34;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/286)
- [User Story 1;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/284)
- [Guardar Lista de Requisitos a Implementar no Repo;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/250)
- [Guardar Lista de Requisitos No Repo;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/203)
- [Lista de Requisitos A Nao Implementar;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/202)
- [Lista de Requisitos de Prioridade Media;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/155)
- [User Story 38;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/137)
- [User Story 28;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/113)
- [User Story 23:](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/90)
- [Atribuicao de Requisitos;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/77)
- [Estudo dos Requisitos do Projeto;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/76)

~ENV

- [Estudo Gitlab;](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/17)


Como membro da equipa de requisitos, a minha função principal era estar a par dos requisitos exigidos pelo cliente, e as suas prioridades, auxiliando a excecução do projeto o melhor possível.
Dentro da minha equipa tive por vezes a função de orientar o trabalho da equipa de requisitos e organizar o seu método de trabalho.

