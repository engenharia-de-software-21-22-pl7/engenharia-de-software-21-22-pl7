# Perfil de Alexandre Leopoldo

- **Nome Completo:** Alexandre Gameiro Leopoldo
- **Número de estudante:** 2019219929
- **Função:**
  - Backend Developer
- **E-mail:** uc2019219929@student.uc.pt
- **Curso:** Licenciatura de Engenharia Informática

<br>

# Lista de Issues

***Env***
- [Estudo GitLab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/12)
- [Estudo Django](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/73)
- [Exploracao da API do Git](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/165)
- [Estudo Arquitetura C4](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/207)

***PM***
- [Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/86)
- [Presenca Reunioes](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/229)

***Dev***
- [Correcao: Barra temporal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/302)

***REQ***
- [User Story 6](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/89)

<br>

# Papel no projeto

Durante as primeiras semanas do projeto, realizei estudos sobre o Git, GitLab, Django, API do GitLab e Arquitetura C4. Após o estudo, fui developer e ajudei a construir o back-end do projeto.