Cátia da Costa Pereira
2019223406

Equipa de requisitos

uc2019223406@student.uc.pt

Licenciatura em Design e Multimédia

Contribuições:

**ENV**

[Estudo Gitlab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/9)

**PM**

[Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/36)
[Presença Reuniões](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/239)

**REQ**

[User Story 3](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/108)
[User Story 4](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/110)
[Lista de Requisitos de Prioridade Media](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/163)
[User Story 3a](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/186)
[Lista de Requisitos a Não Implementar](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/209)
[Ata Reuniao 22/11/2021](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/236)
[Guardar Lista de Possiveis Requisitos a Implementar no Repo](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/253)
[User Story 33](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/259)


**Papel no Projeto**
Fui membro da equipa de requisitos. Realizei tarefas destinadas à organização dos requisitos do projeto.
