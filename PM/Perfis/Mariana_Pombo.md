Mariana Pombo
2019223174

Equipa Frontend 

uc2019223174@student.uc.pt

Licenciatura em Design e Multimédia 


Lista de Issues

ENV 
- [Implementação CSS Listagem Membros UST19 ](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/318)

DES
- [Design Visual Vista Temporal (Atualizacao) ](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/270)
- [Design Visual Vista Temporal UST 3a ](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/191)
- [Design Visual Timeline UST3 ](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/139)


ENV
- [Estudo Django ](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/121)
- [Estudo GitLab ](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/26)

REQ
- [Usertory 30 ](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/112)
- [Usertory 29 ](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/107)
- [Opcoes Logo ](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/72)

PM
- [Presenca Reunioes ](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/235)
- [Perfil Pessoal ](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/40)


Papel no Projeto:
- Fui membro da equipa de frontend. Realizei tarefas destinadas ao desenvolvimento do design visual do projeto. 





