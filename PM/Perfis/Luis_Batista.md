# Perfil de Luís Batista
- **Nome Completo:** Luís Miguel Gomes Batista
- **Número de estudante:** 2019214869
- **Funções:**
    - Backend Developer
- **E-mail:** uc2019214869@student.uc.pt
- **Curso:** Licenciatura em Engenharia Informática

<br>

# Lista de Issues

***Dev***

- [Implementacao Django Vista Temporal UST 3](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/173)
- [Implementacao Django Vista Temporal UST 3a](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/187)
- [Resolver Conflito de Merge](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/244)
- [Implementacao Django Listagem Membros UST 19](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/245)
- [Implementacao Django Listagem Membros UST 19 - versao 2](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/275)
- [Implementacao Escolha do Repositorio a visualizar UST 1](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/294)

***Env***

- [Estudo GitLab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/10)
- [Estudo Django](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/65)
- [Exploracao da API do Git](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/145)
- [Estudo Arquitetura C4](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/197)

***PM***

- [Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/29)
- [Presenca Reunioes](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/232)
- [Atualizacao Perfil Pessoal]()


***REQ***
- [User Story 12](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/88)

<br>

# Papel no projeto

Durante a realização do projeto fui responsável por desenvolver a parte back-end. Nas primeiras semanas estudei as ferramentas e tecnologias necessárias para o desenvolvimento do projeto, tendo usado o tempo restante para as implementações necessárias.