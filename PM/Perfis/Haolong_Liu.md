Haolong Liu
2018288018

Backend Developer

uc2018288018@student.uc.pt

Licenciatura em Engenharia Informática

~PM

- [Perfil Pessoal](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/32)
- [Presença reuniões fora de aula](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/213)

~ARCH

- [Estudo e implementação da arquitetura C4](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/153)

~ENV

- [Estudo Gitlab](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/6)
- [Estudo Django](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/67)
- [Estudo pytest](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/247)

~REQ

- [UST 31](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/100)

Fui membro da equipa de backend, participei no que toca na implementação da arquitetura C4 e ajudei na resolução do contacto entre backend e frontend.
