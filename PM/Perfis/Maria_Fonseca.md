Maria Inês Pereira da Fonseca
2019210452

Equipa Qualidade
Equipa Frontend

uc2019210452@student.uc.pt

Licenciatura em Design e Multimédia 

**Links Úteis**
- [Link para todos os issues que me foram destinados](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues?scope=all&state=all&assignee_username=Ines_Pereira)
- [Link para o KANBAN com o estado das tarefas](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/boards/3300954?scope=all&assignee_username=Ines_Pereira)


~PM
- [Lista Todas as Atas](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#181) [Ficheiro relacionado](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/merge_requests/48)
- [Estrutura do Repo](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#4)
- [Perfil Pessoal](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#37) [Ficheiro relacionado](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/merge_requests/18)
- [Localizacao ficheiros com paths](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#152)[Ficheiro Relacionado](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7!47)
- [Ata Reuniao 13/11/2021](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#179)
- [Presenca em Reunioes](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#194)
- [Ata Reuniao 24/11/2021](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#248)
- [Ata Runiao 30/11/2021](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#265)
...

~ARCH
- [Proposta Arquitetura em C4](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#92)

~DES
- [Teste Blackbox login ust38](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#288)
- [Teste Design ust 38](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#264)
- [Teste Design ust3a](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#287)

~PROC
- [Teste Blackbox ust 3a](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#288)
- [Teste Design ust3a](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#287)
- [Teste Design ust 38](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#264)
- [Comentario a Desenho de Teste da Validacao do projeto da outra equipa parte 1](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#329)

~DEV
- [Implementacao HTML CSS Vista Temporal UST 3](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#171)~[Commit do código](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/merge_requests/49)

~ENV
- [Estudo Python](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#129)
- [Estudo Django](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#81)
- [Estudo Conceitos Qualidade](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#46)
- [Estudo Gitlab](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#7)
- [Estudo Arquitetura em c4](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#92)

~QA 
- [Teste Blackbox login ust38](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#288)
- [Revisao Ligacao entre ficheiros e issues](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#127)
- [Revisao estudo django](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#122)
- [Revisao Perfis Pessoais](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#93)
- [Revisao Template Perfil](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#57)
- [Revisao Template Ata](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/56)
- [Revisao Template Issues](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/55)
...

~REQ
- [UST 2](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#274)
- [UST 10](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/273)
- [UST 39](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/75)
- [Distribuicao tarefas UST3 E 38](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/172)
- [Teste Blackbox login ust38](engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7#288)
- [Design Visual UST 38](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/140)
- [Opcoes Logo](https://gitlab.com/engenharia-de-software-21-22-pl7/engenharia-de-software-21-22-pl7/-/issues/47)

Fui membro da equipa de qualidade e de frontend tendo realizado testes e também tarefas de outras categorias. 
